// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class CPLUSPLUS_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
		// Blueprint accessible method.
		UFUNCTION(BlueprintCallable, Category = "Karma Libraries")
		static float getCircleAreaDLL(float radius);
	
	
	
	
};
