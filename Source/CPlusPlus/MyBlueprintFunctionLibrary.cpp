// Fill out your copyright notice in the Description page of Project Settings.

#include "CPlusPlus.h"
#include "MyBlueprintFunctionLibrary.h"
#include <string>

typedef float(*_getCircleArea)(float radius); // Declare the DLL function.

typedef std::wstring(*_greeting)(); // Declare the DLL function.


float UMyBlueprintFunctionLibrary::getCircleAreaDLL(float radius)
{
	FString filePath = FPaths::Combine(*FPaths::GamePluginsDir(), TEXT("KarmasDLLs/"), TEXT("Win32Project3.dll")); // Concatenate the plugins folder and the DLL file.
	GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Path to DLL is ") + filePath);
	if (FPaths::FileExists(filePath))
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Dll file found"));
		void *DLLHandle;
		DLLHandle = FPlatformProcess::GetDllHandle(*filePath); // Retrieve the DLL.
		if (DLLHandle != NULL)
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Dll handle OK"));
			// Looking for first function 

			float out = 0;
			_getCircleArea DLLgetCircleArea = NULL; // Local DLL function pointer.
			FString procName = "getCircleArea"; // The exact name of the DLL function.
			DLLgetCircleArea = (_getCircleArea)FPlatformProcess::GetDllExport(DLLHandle, *procName); // Export the DLL function.
			if (DLLgetCircleArea != NULL)
			{
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("First Function found"));
				out = DLLgetCircleArea(radius); // Call the DLL function, with arguments corresponding to the signature and return type of the function.
				//return out; // return to UE
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("First Function NOT found!!!"));
			}

			// Looking for second function
			_greeting DLLGreeting = NULL; // Local DLL function pointer.
			FString procName2 = "greeting"; // The exact name of the DLL function.
			DLLGreeting = (_greeting)FPlatformProcess::GetDllExport(DLLHandle, *procName2); // Export the DLL function.
			if (DLLGreeting != NULL)
			{
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Second Function found"));
				
				
				std::wstring t1 =L"������***";
				//FString t2(t1.c_str());
				//GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("-----")+ t2);

				std::wstring result = DLLGreeting();
				FString resultStr(result.c_str());
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Result from TXT FUNCTION "+ resultStr));
				
				return out; // return to UE
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Second Function NOT found!!!"));
			}
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Dll handle NOT OK!!!"));
		}
	}
	else {
		GEngine->AddOnScreenDebugMessage(-1, 15.f, FColor::Green, TEXT("Dll file NOT found!!!"));
	}
	return 1.00f;
}


