// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>



// TODO: reference additional headers your program requires here
#include <string>

extern "C"
{
	__declspec(dllexport) float getCircleArea(float radius);
	__declspec(dllexport) std::wstring greeting();
	std::string ANSItoUTF16le(const char* v_str);
}